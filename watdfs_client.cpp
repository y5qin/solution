#include "watdfs_client.h"
#include "debug.h"
INIT_LOG

#include "rpc.h"

#include <fcntl.h>
#include <iostream>
#include <map>
#include <sys/stat.h>
#include <unistd.h>

std::map<const char *, int> file_status;
std::map<const char *, time_t> Tc;

struct params
{
    const char *path_to_cache;
    time_t cache_interval;
};

void *watdfs_cli_init(struct fuse_conn_info *conn, const char *path_to_cache, time_t cache_interval, int *ret_code)
{
    if (rpcClientInit() != 0)
    {
        *ret_code = -errno;
        return nullptr;
    }

    void *userdata = malloc(sizeof(params));
    ((params *)userdata)->path_to_cache = path_to_cache;
    ((params *)userdata)->cache_interval = cache_interval;

    *ret_code = 0;
    return userdata;
}

void watdfs_cli_destroy(void *userdata)
{
    free(userdata);
    rpcClientDestroy();
}

char *get_full_path(void *userdata, const char *short_path)
{
    int short_path_len = strlen(short_path);
    const char *path_to_cache = ((params *)userdata)->path_to_cache;
    int dir_len = strlen(path_to_cache);
    int full_len = dir_len + short_path_len + 1;

    char *full_path = (char *)malloc(full_len);

    strcpy(full_path, path_to_cache);
    strcat(full_path, short_path);

    return full_path;
}

int rpc_getattr(const char *path, struct stat *statbuf)
{
    DLOG("rpc_getattr called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct stat);
    args[1] = (void *)statbuf;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"getattr", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("getattr rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    if (fxn_ret < 0)
    {
        memset(statbuf, 0, sizeof(struct stat));
    }

    delete[] args;
    return fxn_ret;
}

int rpc_mknod(const char *path, mode_t mode, dev_t dev)
{
    DLOG("rpc_mknod called for '%s'", path);

    int ARG_COUNT = 4;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_INPUT) | (ARG_INT << 16u);
    args[1] = (void *)&mode;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    args[2] = (void *)&dev;
    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[3] = (void *)&retcode;
    arg_types[4] = 0;

    int rpc_ret = rpcCall((char *)"mknod", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("mknod rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;
}

int rpc_open(const char *path, struct fuse_file_info *fi)
{
    DLOG("rpc_open called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
    args[1] = (void *)fi;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"open", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("open rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;
}

int rpc_release(const char *path, struct fuse_file_info *fi)
{
    DLOG("rpc_release called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;

    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
    args[1] = (void *)fi;

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;

    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"release", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("release rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;
}

int rpc_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    DLOG("rpc_read called for '%s'", path);

    int ARG_COUNT = 6;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
    args[4] = (void *)fi;
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[5] = (void *)&retcode;
    arg_types[6] = 0;

    int fxn_ret = 0;
    int num_calls = size / MAX_ARRAY_LEN + 1;
    size_t size_i;
    for (int i = 0; i < num_calls; i++)
    {
        if (i == num_calls - 1)
        {
            size_i = size % MAX_ARRAY_LEN;
        }
        else
        {
            size_i = MAX_ARRAY_LEN;
        }

        arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size_i;
        args[1] = (void *)(buf + i * MAX_ARRAY_LEN);
        args[2] = (void *)&size_i;
        off_t offset_i = offset + i * MAX_ARRAY_LEN;
        args[3] = (void *)&offset_i;

        int rpc_ret = rpcCall((char *)"read", arg_types, args);

        if (rpc_ret < 0)
        {
            DLOG("read rpc failed with error '%d'", rpc_ret);
            fxn_ret = -EINVAL;
        }
        else
        {
            fxn_ret += retcode;
        }

        if (fxn_ret < 0)
        {
            break;
        }
    }

    delete[] args;
    return fxn_ret;
}

int rpc_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    DLOG("rpc_write called for '%s'", path);

    int ARG_COUNT = 6;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
    args[4] = (void *)fi;
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[5] = (void *)&retcode;
    arg_types[6] = 0;

    int fxn_ret = 0;
    int num_calls = size / MAX_ARRAY_LEN + 1;
    size_t size_i;
    for (int i = 0; i < num_calls; i++)
    {
        if (i == num_calls - 1)
        {
            size_i = size % MAX_ARRAY_LEN;
        }
        else
        {
            size_i = MAX_ARRAY_LEN;
        }

        arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size_i;
        args[1] = (void *)(buf + i * MAX_ARRAY_LEN);
        args[2] = (void *)&size_i;
        off_t offset_i = offset + i * MAX_ARRAY_LEN;
        args[3] = (void *)&offset_i;

        int rpc_ret = rpcCall((char *)"write", arg_types, args);

        if (rpc_ret < 0)
        {
            DLOG("write rpc failed with error '%d'", rpc_ret);
            fxn_ret = -EINVAL;
        }
        else
        {
            fxn_ret += retcode;
        }

        if (fxn_ret < 0)
        {
            break;
        }
    }

    delete[] args;
    return fxn_ret;
}

int rpc_truncate(const char *path, off_t newsize)
{
    DLOG("rpc_truncate called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    args[1] = (void *)&newsize;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"truncate", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("truncate rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;

    return 0;
}

int rpc_fsync(const char *path, struct fuse_file_info *fi)
{
    DLOG("rpc_fsync called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
    args[1] = (void *)fi;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"fsync", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("fsync rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;
}

int rpc_utimensat(const char *path, const struct timespec ts[2])
{
    DLOG("rpc_utimensat called for '%s'", path);

    int ARG_COUNT = 3;
    void **args = new void *[ARG_COUNT];
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)pathlen;
    args[0] = (void *)path;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)(2 * sizeof(struct timespec));
    args[1] = (void *)ts;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode;
    args[2] = (void *)&retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"utimensat", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0)
    {
        DLOG("utimensat rpc failed with error '%d'", rpc_ret);
        fxn_ret = -EINVAL;
    }
    else
    {
        fxn_ret = retcode;
    }

    delete[] args;
    return fxn_ret;
}

int download(void *userdata, const char *path)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("download called for '%s'", full_path);

    struct stat file_stat;
    if (rpc_getattr(path, &file_stat) < 0)
    {
        DLOG("Remote file does not exist.");
        return -errno;
    }
    size_t file_size = file_stat.st_size;
    char *buf = new char[file_size];

    struct fuse_file_info server_fi;
    server_fi.flags = O_RDONLY;
    if (truncate(full_path, 0) < 0)
    {
        return -errno;
    };
    if (rpc_open(path, &server_fi) < 0)
    {
        return -errno;
    }
    if (rpc_read(path, buf, file_size, 0, &server_fi) < 0)
    {
        return -errno;
    }
    if (rpc_release(path, &server_fi) < 0)
    {
        return -errno;
    }

    int fd = open(full_path, O_WRONLY | O_EXCL);
    if (fd < 0)
    {
        return -errno;
    }
    if (write(fd, buf, file_size) < 0)
    {
        return -errno;
    }
    if (close(fd) < 0)
    {
        return -errno;
    }

    Tc[path] = time(0);

    struct timespec ts[2];
    ts[0].tv_nsec = file_stat.st_atim.tv_nsec;
    ts[1].tv_nsec = file_stat.st_mtim.tv_nsec;
    if (utimensat(0, full_path, ts, 0) < 0)
    {
        return -errno;
    }

    return 0;
}

int upload(void *userdata, const char *path)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("upload called for '%s'", full_path);

    struct stat file_stat;
    if (stat(full_path, &file_stat) < 0)
    {
        return -errno;
    }
    size_t file_size = file_stat.st_size;
    char *buf = new char[file_size];

    int fd = open(full_path, O_RDONLY);
    if (fd < 0)
    {
        return -errno;
    }
    if (read(fd, buf, file_size) < 0)
    {
        return -errno;
    }
    if (close(fd) < 0)
    {
        return -errno;
    }

    struct fuse_file_info server_fi;
    server_fi.flags = O_WRONLY;
    if (rpc_truncate(path, 0) < 0)
    {
        return -errno;
    }
    if (rpc_open(path, &server_fi) < 0)
    {
        return -errno;
    }
    if (rpc_write(path, buf, file_size, 0, &server_fi) < 0)
    {
        return -errno;
    }
    if (rpc_release(path, &server_fi) < 0)
    {
        return -errno;
    }

    Tc[path] = time(0);

    struct timespec ts[2];
    ts[0].tv_nsec = file_stat.st_atim.tv_nsec;
    ts[1].tv_nsec = file_stat.st_mtim.tv_nsec;

    if (rpc_utimensat(path, ts) < 0)
    {
        return -errno;
    }

    return 0;
}

bool check_freshness(void *userdata, const char *path)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("check_freshness called for '%s'", full_path);

    time_t T = time(0);
    time_t t = ((params *)userdata)->cache_interval;

    if (T - Tc[path] < t)
    {
        return true;
    }

    struct stat client_stat, server_stat;
    if (stat(full_path, &client_stat) < 0)
    {
        return -errno;
    }
    if (rpc_getattr(path, &server_stat) < 0)
    {
        return -errno;
    }

    time_t T_client = client_stat.st_mtim.tv_sec;
    time_t T_server = server_stat.st_mtim.tv_sec;
    if (T_client == T_server)
    {
        Tc[path] = time(0);
        return true;
    }
    else
    {
        client_stat.st_mtim.tv_sec = T_server;
        return false;
    }
}

int watdfs_cli_getattr(void *userdata, const char *path, struct stat *statbuf)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_getattr called for '%s'", full_path);

    if (stat(full_path, statbuf) < 0)
    {
        DLOG("Local file does not exist.");

        if (rpc_getattr(path, statbuf) < 0)
        {
            DLOG("Remote file does not exist.");
            return -errno;
        }

        if (mknod(full_path, 0777, 0))
        {
            return -errno;
        }
    }

    if (file_status.find(path) == file_status.end() || file_status[path] == -1)
    {
        if (download(userdata, path) != 0)
        {
            return -errno;
        }
    }
    else if (file_status[path] == 0)
    {
        if (!check_freshness(userdata, path))
        {
            if (download(userdata, path) != 0)
            {
                return -errno;
            }
        }
    }

    if (stat(full_path, statbuf) < 0)
    {
        return -errno;
    }

    return 0;
}

int watdfs_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_mknod called for '%s'", full_path);

    if (mknod(full_path, mode, dev) < 0)
    {
        return -errno;
    }
    
    if (rpc_mknod(path, 0777, dev) < 0)
    {
        return -errno;
    }

    file_status[path] = -1;

    return 0;
}

int watdfs_cli_open(void *userdata, const char *path, struct fuse_file_info *fi)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_open called for '%s'", full_path);

    if (file_status.find(path) != file_status.end() && file_status[path] != -1)
    {
        return -EMFILE;
    }

    if (download(userdata, path) < 0)
    {
        return -errno;
    }

    int sys_ret = open(full_path, fi->flags);
    if (sys_ret < 0)
    {
        return -errno;
    }
    fi->fh = sys_ret;

    if ((fi->flags & O_ACCMODE) == O_RDONLY)
    {
        file_status[path] = 0;
    }
    else if ((fi->flags & O_ACCMODE) == O_WRONLY || (fi->flags & O_ACCMODE)  == O_RDWR)
    {
        file_status[path] = 1;
    }

    return 0;
}

int watdfs_cli_release(void *userdata, const char *path, struct fuse_file_info *fi)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_release called for '%s'", full_path);

    if ((fi->flags & O_ACCMODE)  == O_WRONLY || (fi->flags & O_ACCMODE)  == O_RDWR)
    {
        if (upload(userdata, path) < 0)
        {
            return -errno;
        }
    }

    if (close(fi->fh) < 0)
    {
        return -errno;
    }

    file_status[path] = -1;
    return 0;
}

int watdfs_cli_read(void *userdata, const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_read called for '%s'", full_path);

    if (!check_freshness(userdata, path))
    {
        if (download(userdata, path) < 0)
        {
            return -errno;
        }
    }

    int sys_ret = pread(fi->fh, buf, size, offset);
    if (sys_ret < 0)
    {
        return -errno;
    }

    return sys_ret;
}

int watdfs_cli_write(void *userdata, const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_write called for '%s'", full_path);

    int sys_ret = write(fi->fh, buf, size);
    if (sys_ret < 0)
    {
        return -errno;
    }

    if (!check_freshness(userdata, path))
    {
        if (upload(userdata, path) < 0)
        {
            return -errno;
        }
    }

    return sys_ret;
}

int watdfs_cli_truncate(void *userdata, const char *path, off_t newsize)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_truncate called for '%s'", full_path);

    if (file_status.find(path) == file_status.end() || file_status[path] == -1)
    {
        if (download(userdata, path) < 0)
        {
            return -errno;
        }
        if (truncate(full_path, newsize) < 0)
        {
            return -errno;
        }
        if (upload(userdata, path) < 0)
        {
            return -errno;
        }
    }
    else if (file_status[path] == 0)
    {
        return -EMFILE;
    }
    else
    {
        if (truncate(full_path, newsize) < 0)
        {
            return -errno;
        }
        if (!check_freshness(userdata, path))
        {
            if (upload(userdata, path) < 0)
            {
                return -errno;
            }
        }
    }

    return 0;
}

int watdfs_cli_fsync(void *userdata, const char *path, struct fuse_file_info *fi)
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_fsync called for '%s'", full_path);

    if (file_status.find(path) != file_status.end() && file_status[path] == 0)
    {
        return -errno;
    }

    if (fsync(fi->fh) < 0)
    {
        return -errno;
    }

    if (upload(userdata, path), 0)
    {
        return -errno;
    }

    return 0;
}

int watdfs_cli_utimensat(void *userdata, const char *path, const struct timespec ts[2])
{
    char *full_path = get_full_path(userdata, path);
    DLOG("watdfs_cli_utimensat called for '%s'", full_path);

    if (file_status.find(path) == file_status.end() || file_status[path] == -1)
    {
        if (download(userdata, path) < 0)
        {
            return -errno;
        }
        if (utimensat(AT_FDCWD, full_path, ts, 0) < 0)
        {
            return -errno;
        }
        if (upload(userdata, path) < 0)
        {
            return -errno;
        }
    }
    else if (file_status[path] == 0)
    {
        return -EMFILE;
    }
    else
    {
        if (utimensat(AT_FDCWD, full_path, ts, 0) < 0)
        {
            return -errno;
        }
        if (!check_freshness(userdata, path))
        {
            if (utimensat(AT_FDCWD, full_path, ts, 0) < 0)
            {
                return -errno;
            }
        }
    }

    return 0;
}
