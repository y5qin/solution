#include "rpc.h"
#include "debug.h"
INIT_LOG

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <cstdlib>

#include <fcntl.h>
#include <fuse.h>
#include <iostream>
#include <map>

char *server_persist_dir = nullptr;

// Important: the server needs to handle multiple concurrent client requests.
// You have to be carefuly in handling global variables, esp. for updating them.
// Hint: use locks before you update any global variable.

std::map<const char *, int> file_status;

// We need to operate on the path relative to the the server_persist_dir.
// This function returns a path that appends the given short path to the
// server_persist_dir. The character array is allocated on the heap, therefore
// it should be freed after use.
// Tip: update this function to return a unique_ptr for automatic memory management.
char *get_full_path(char *short_path)
{
    int short_path_len = strlen(short_path);
    int dir_len = strlen(server_persist_dir);
    int full_len = dir_len + short_path_len + 1;

    char *full_path = (char *)malloc(full_len);

    strcpy(full_path, server_persist_dir);
    strcat(full_path, short_path);
    DLOG("Full path: %s\n", full_path);

    return full_path;
}

int watdfs_getattr(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    struct stat *statbuf = (struct stat *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = stat(full_path, statbuf);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_mknod(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    mode_t mode = *(mode_t *)args[1];
    dev_t dev = *(dev_t *)args[2];
    int *ret = (int *)args[3];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = mknod(full_path, mode, dev);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_open(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = (fuse_file_info *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    if (file_status[short_path] == 1 && ((fi->flags & O_ACCMODE) == O_WRONLY || (fi->flags & O_ACCMODE) == O_RDWR))
    {
        return -EACCES;
    }

    *ret = 0;
    int sys_ret = open(full_path, fi->flags);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }
    fi->fh = sys_ret;

    if ((fi->flags & O_WRONLY) || (fi->flags & O_RDWR))
    {
        file_status[short_path] = 1;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_release(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = (fuse_file_info *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = close(fi->fh);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    file_status[short_path] = 0;

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_read(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    char *buf = (char *)args[1];
    size_t *size = (size_t *)args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = (fuse_file_info *)args[4];
    int *ret = (int *)args[5];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = pread(fi->fh, buf, *size, *offset);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }
    else
    {
        *ret = sys_ret;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_write(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    char *buf = (char *)args[1];
    size_t *size = (size_t *)args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = (fuse_file_info *)args[4];
    int *ret = (int *)args[5];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = pwrite(fi->fh, buf, *size, *offset);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }
    else
    {
        *ret = sys_ret;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_truncate(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    off_t *newsize = (off_t *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = truncate(full_path, *newsize);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_fsync(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = (fuse_file_info *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = fsync(fi->fh);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int watdfs_utimensat(int *argTypes, void **args)
{
    char *short_path = (char *)args[0];
    struct timespec *ts = (timespec *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    int sys_ret = utimensat(AT_FDCWD, full_path, ts, 0);
    if (sys_ret < 0)
    {
        *ret = -errno;
    }

    free(full_path);
    DLOG("Returning code: %d", *ret);
    return 0;
}

int main(int argc, char *argv[])
{
    // argv[1] should contain the directory where you should store data on the
    // server. If it is not present it is an error, that we cannot recover from.
    if (argc != 2)
    {
        // In general you shouldn't print to stderr or stdout, but it may be
        // helpful here for debugging. Important: Make sure you turn off logging
        // prior to submission!
        // See watdfs_client.c for more details
        // # ifdef PRINT_ERR
        // std::cerr << "Usaage:" << argv[0] << " server_persist_dir";
        // #endif
        return -1;
    }
    // Store the directory in a global variable.
    server_persist_dir = argv[1];

    // TODO: Initialize the rpc library by calling `rpcServerInit`.
    // Important: `rpcServerInit` prints the 'export SERVER_ADDRESS' and
    // 'export SERVER_PORT' lines. Make sure you *do not* print anything
    // to *stdout* before calling `rpcServerInit`.
    DLOG("Initializing server...");
    int ret = rpcServerInit();

    // TODO: If there is an error with `rpcServerInit`, it maybe useful to have
    // debug-printing here, and then you should return.
    if (ret != 0)
    {
        return ret;
    }

    // TODO: Register your functions with the RPC library.
    // Note: The braces are used to limit the scope of `argTypes`, so that you can
    // reuse the variable for multiple registrations. Another way could be to
    // remove the braces and use `argTypes0`, `argTypes1`, etc.
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"getattr", argTypes, watdfs_getattr);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[5];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (ARG_INT << 16u);
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[4] = 0;

        ret = rpcRegister((char *)"mknod", argTypes, watdfs_mknod);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"open", argTypes, watdfs_open);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"release", argTypes, watdfs_release);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[7];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[6] = 0;

        ret = rpcRegister((char *)"read", argTypes, watdfs_read);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[7];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[6] = 0;

        ret = rpcRegister((char *)"write", argTypes, watdfs_write);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"truncate", argTypes, watdfs_truncate);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)sizeof(struct fuse_file_info);
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"fsync", argTypes, watdfs_fsync);
        if (ret < 0)
        {
            return ret;
        }
    }
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)(2 * sizeof(struct timespec));
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"utimensat", argTypes, watdfs_utimensat);
        if (ret < 0)
        {
            return ret;
        }
    }

    rpcExecute();

    return ret;
}
